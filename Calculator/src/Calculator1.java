import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class Calculator1 extends JFrame {

    private JButton sin;
    private JButton cos;
    private JButton tan;
    private JButton ctg;
    private JButton sqrt;
    private JButton epow;
    private JButton xsq;
    private double x;

    public Calculator1() {

        super("Operatii cu 1 numar");
        setLayout(new FlowLayout());

        sin = new JButton("sin");
        add(sin);

        cos = new JButton("cos");
        add(cos);

        tan = new JButton("tan");
        add(tan);

        ctg = new JButton("ctg");
        add(ctg);

        sqrt = new JButton("sqrt");
        add(sqrt);

        epow = new JButton("x^e");
        add(epow);

        xsq = new JButton("x^2");
        add(xsq);

        String fn = JOptionPane.showInputDialog("Enter the number");

        x = Double.parseDouble(fn);

        HandlingClass handlingClass = new HandlingClass();
        sin.addActionListener(handlingClass);
        cos.addActionListener(handlingClass);
        tan.addActionListener(handlingClass);
        ctg.addActionListener(handlingClass);
        sqrt.addActionListener(handlingClass);
        epow.addActionListener(handlingClass);
        xsq.addActionListener(handlingClass);


    }

    private class HandlingClass implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            String string = "";

            if (event.getSource() == sin)
                string = String.format("The result is %f", Operatii.sin(x));

            else if (event.getSource() == cos)
                string = String.format("The result is %f", Operatii.cos(x));

            else if (event.getSource() == tan)
                string = String.format("The result is %f", Operatii.tg(x));

            else if (event.getSource() == ctg)
                string = String.format("The result is %f", Operatii.ctg(x));

            else if (event.getSource() == sqrt)
                string = String.format("The result is %f", Operatii.sqrt(x));

            else if (event.getSource() == epow)
                string = String.format("The result is %f", Operatii.epow(x));

            else if (event.getSource() == sin)
                string = String.format("The result is %f", Operatii.square(x));

            JOptionPane.showMessageDialog(null, string);
            dispose();
        }
    }

}
