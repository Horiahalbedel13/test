import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class Calculator2 extends JFrame {


    private JButton sum;
    private JButton dif;
    private JButton div;
    private JButton pow;
    private JButton mod;
    private double x;
    private double y;

    public Calculator2() {

        super("Operatii cu 2 numere");
        setLayout(new FlowLayout());

        sum = new JButton("+");
        add(sum);

        dif = new JButton("-");
        add(dif);

        div = new JButton("÷");
        add(div);

        pow = new JButton("^");
        add(pow);

        mod = new JButton("%");
        add(mod);


        String fn = JOptionPane.showInputDialog("Enter the first number");
        String fn2 = JOptionPane.showInputDialog("Enter the second number");

        x = Double.parseDouble(fn);
        y = Double.parseDouble(fn2);


        HandlingClass2 handlingClass2 = new HandlingClass2();
        sum.addActionListener(handlingClass2);
        dif.addActionListener(handlingClass2);
        div.addActionListener(handlingClass2);
        pow.addActionListener(handlingClass2);
        mod.addActionListener(handlingClass2);


    }

    private class HandlingClass2 implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            String string2 = "";

            if (event.getSource() == sum)
                string2 = String.format("The result is %f", Operatii.sum(x, y));

            else if (event.getSource() == dif)
                string2 = String.format("The result is %f", Operatii.dif(x, y));

            else if (event.getSource() == div)
                string2 = String.format("The result is %f", Operatii.div(x, y));

            else if (event.getSource() == pow)
                string2 = String.format("The result is %f", Operatii.pow(x, y));

            else if (event.getSource() == mod)
                string2 = String.format("The result is %f", Operatii.mod(x, y));


            JOptionPane.showMessageDialog(null, string2);
            dispose();
        }
    }

}


