public class Operatii {

    public static double sin(double x) {
        return Math.sin(x);
    }

    public static double cos(double x) {
        return Math.cos(x);
    }

    public static double tg(double x) {
        return Math.tan(x);
    }

    public static double ctg(double x) {
        return 1 / Math.tan(x);
    }

    public static double sqrt(double x) {
        return Math.sqrt(x);
    }

    public static double epow(double x) {
        return Math.pow(x ,Math.E);
    }

    public static double square(double x) {
        return Math.pow(x, 2);
    }

    public static double sum(double a, double b) {
        return a + b;
    }

    public static double dif(double a, double b) {
        return a - b;
    }

    public static double div(double a, double b) {
        return a / b;
    }

    public static double pow(double a, double b) {
        return Math.pow(a, b);
    }

    public static double mod(double a, double b) {
        return a % b;
    }

}
