import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

public class Meniu extends JFrame {

    private JButton nr1;
    private JButton nr2;

    public Meniu() {

        super("Meniu");
        setLayout(new FlowLayout());

        nr1 = new JButton("1 number");
        add(nr1);

        nr2 = new JButton("2 numbers");
        add(nr2);

        TheHandler theHandler = new TheHandler();
        nr1.addActionListener(theHandler);
        nr2.addActionListener(theHandler);
    }

    private class TheHandler implements ActionListener {

        Calculator1 calculator1;
        Calculator2 calculator2;

        public void actionPerformed(ActionEvent event) {
            if (event.getSource() == nr1) {
                calculator1 = new Calculator1();
                calculator1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                calculator1.setSize(350, 200);
                calculator1.setVisible(true);
            }

            if (event.getSource() == nr2) {
                calculator2 = new Calculator2();
                calculator2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                calculator2.setSize(350, 200);
                calculator2.setVisible(true);
            }

        }
    }
}


