import java.util.ArrayList;

public class CDCatalog {
    private ArrayList<CD> cds = new ArrayList<>();
    private int i = cds.size();

    public void removeCD(CD cd) {
        cds.remove(cd);
    }

    public void addCd(CD cd) {
        cds.add(i, cd);
    }

    public CD searchByTitle(String title) {


        for (CD cd : cds) {
            if (cd.getTitle() == title)
                return cd;
        }
        return null;
    }

    public CD searchByArtist(Artist artist) {

        for (CD cd : cds) {
            if (cd.getArtist() == artist.getName())
                return cd;
        }
        return null;
    }

    public void removeAllCDs() {
        cds.clear();
    }

    public CD getByIndex(int i) {
        return cds.get(i);
    }

    public int getSize() {
        return cds.size();
    }


}