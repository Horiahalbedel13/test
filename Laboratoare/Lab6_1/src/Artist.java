public class Artist {
    private String name;

    public String getName() {
            return this.name;

    }

    public Artist(String name) {
        this.name = name;
    }

    public Artist() {
        this.name = "No Artist";
    }
}