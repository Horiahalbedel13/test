public class CDApp {
    public static void main(String[] args) {
        Artist artist1 = new Artist("a1");
        Artist artist2 = new Artist("a2");
        Artist artist3 = new Artist("a3");
        Artist artist4 = new Artist("a4");

        CD cd1 = new CD("titlu1", artist1);
        CD cd2 = new CD("titlu2", artist2);
        CD cd3 = new CD("titlu3", artist4);
        CD cd4 = new CD(artist4);

        CDCatalog cdCatalog = new CDCatalog();
        cdCatalog.addCd(cd1);
        cdCatalog.addCd(cd2);
        cdCatalog.addCd(cd3);
        cdCatalog.removeCD(cd1);

        if (cdCatalog.searchByTitle("titlu3") != null)
            System.out.println(cdCatalog.searchByTitle("titlu3").getArtist());

        if (cdCatalog.searchByArtist(artist3) != null)
            System.out.println(cdCatalog.searchByArtist(artist3).getTitle());


    }
}