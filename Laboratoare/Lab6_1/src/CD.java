public class CD {

    private String title;
    private Artist artist;


    public String getTitle() {
        return this.title;
    }

    public String getArtist() {
        return this.artist.getName();

    }

    public CD(String title, Artist artist) {
        this.title = title;
        this.artist = artist;
    }


    public CD(String title) {
        this.title = title;
    }

    public CD() {
        this.title = "Empty";
    }

    public CD(Artist artist) {
        this.artist = artist;
        this.title = "No title";
    }

}