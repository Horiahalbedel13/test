public class Instructor extends Person {
    private String instructorID;
    private Course course;

    public Instructor(String name, String address, String instructorID) {
        super(name, address);
        this.instructorID = instructorID;
    }

    public Instructor(String instructorID) {
        super("NoName", "NoAddress");
        this.instructorID = instructorID;
    }

    public Instructor(String name, String address, String instructorID, Course course) {
        super(name, address);
        this.instructorID = instructorID;
        this.course = course;
    }

    public Instructor(){
        super("NoName", "NoAddress");
        this.instructorID = "NoID";
    }


}
