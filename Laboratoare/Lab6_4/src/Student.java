public class Student extends Person {
    private long studentID;

    public Student(String name, String address, String studentID) {
        super(name, address);
        this.studentID = 0;
    }

    public Student(String studentID) {
        super("NoName", "NoAddress");
        this.studentID = 0;
    }

    public Student(){
        super("NoName", "NoAddress");
        this.studentID = 0;
    }
}
