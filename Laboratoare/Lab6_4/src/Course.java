import java.util.ArrayList;

public class Course {
    private String name;
    private String courseID;
    private Instructor instructor;
    private ArrayList<Student> students = new ArrayList<>();

    public Course(String name, String courseID, Instructor instructor, ArrayList<Student> students) {
        this.name = name;
        this.courseID = courseID;
        this.instructor = instructor;
        this.students = students;
    }

    public Course(String name, String courseID, Instructor instructor) {
        this.name = name;
        this.courseID = courseID;
        this.instructor = instructor;
    }

    public Course(){
        this.name = "NoName";
        this.instructor = new Instructor();
        this.courseID = "NoCourseID";

    }
}
