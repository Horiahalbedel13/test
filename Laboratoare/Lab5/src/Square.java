public class Square extends Rectanlge {

    public Square() {
        super();
    }

    public Square(double width, double length) {
        super(width, length);
    }

    public Square(String color, boolean filled, double width, double length) {
        super(color, filled, width, length);
    }

    public void setSide(double length) {
        this.length = length;
        this.width = length;
    }

    public double getSide() {
        return this.length;
    }

    public double getArea() {
        return this.length * this.width;
    }

    public double getPerimeter() {
        return this.length * 4;
    }

    public String toString() {
        return String.format("A Square with side=%f, which is a subclass of %s", this.length, super.toString());
    }


}
