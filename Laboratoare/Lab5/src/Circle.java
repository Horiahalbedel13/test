public class Circle extends Shape {
    double radius;

    public Circle() {
        super();
        this.radius = 1.0;
    }

    public Circle(int radius) {
        super();
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*this.radius*this.radius;
    }

    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    public String toString(){
        return String.format("A Circle with radius=%f, which is a subclass of %s", this.radius, super.toString());
    }
}
