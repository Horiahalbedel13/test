public class Rectanlge extends Shape {
    double length;
    double width;




    public Rectanlge(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectanlge() {
        this.width = 1.0;
        this.length = 1.0;
    }

    public Rectanlge(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return this.width*this.length;
    }

    public double getPerimeter(){
        return 2*(this.length + this.width);
    }
    public String toString(){
        return String.format("A Rectangle with width=%f and length=%f, which is a subclass of %s", this.width, this.length, super.toString());
    }
}
