public class Main {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("o1", 213);
        BankAccount bankAccount1 = new BankAccount("o1", 213);
        BankAccount bankAccount2 = new BankAccount("o2", 243);

        System.out.println(bankAccount.equals(bankAccount1));
        System.out.println(bankAccount1.equals(bankAccount2));
        System.out.println(bankAccount1.hashCode());
        System.out.println(bankAccount.hashCode());
        System.out.println(bankAccount2.hashCode());


    }
}
