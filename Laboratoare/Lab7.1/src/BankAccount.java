public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        this.balance = this.balance - amount;
    }

    public void deposit(double amount){
        this.balance = this.balance + amount;
    }

    @Override
    public boolean equals(Object object) {
        if(object instanceof BankAccount){
            BankAccount bankAccount = (BankAccount)object;
            return(this.balance == bankAccount.balance && this.owner.equals(bankAccount.owner));
        }
        else
            return false;
    }

    @Override
    public int hashCode() {
        return (int)balance + owner.hashCode();
    }
}
