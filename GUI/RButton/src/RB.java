import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class RB extends JFrame {

    private JTextField textField;
    private Font pf;
    private Font bf;
    private Font itf;
    private Font bif;
    private JRadioButton pb;
    private JRadioButton bb;
    private JRadioButton ib;
    private JRadioButton bib;
    private ButtonGroup buttonGroup;

    public RB(){
        super("App");
        setLayout(new FlowLayout());

        textField = new JTextField("Text", 20);
        add(textField);

        pb = new JRadioButton("plain", true);
        bb = new JRadioButton("bold", false);
        ib = new JRadioButton("italic", false);
        bib = new JRadioButton("bold and italic", false);
        add(pb);
        add(bb);
        add(ib);
        add(bib);

        buttonGroup = new ButtonGroup();
        buttonGroup.add(pb);
        buttonGroup.add(bb);
        buttonGroup.add(ib);
        buttonGroup.add(bib);

        pf = new Font("Serif", Font.PLAIN, 10);
        bf = new Font("Serif", Font.BOLD, 10);
        itf = new Font("Serif", Font.ITALIC, 10);
        bif = new Font("Serif", Font.BOLD + Font.ITALIC, 10);

        textField.setFont(pf);

        pb.addItemListener(new HandlrCls(pf));
        bb.addItemListener(new HandlrCls(bf));
        ib.addItemListener(new HandlrCls(itf));
        bib.addItemListener(new HandlrCls(bif));

    }

    private class HandlrCls implements ItemListener{
        private  Font font;

        public HandlrCls(Font f){
            font = f;
        }

        public void itemStateChanged(ItemEvent event){
            textField.setFont(font);
        }

    }


}
